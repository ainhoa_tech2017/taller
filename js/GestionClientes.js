var clientesObtenidos;
var flagUrl="https://www.countries-ofthe-world.com/flags-normal/flag-of-";

function getClientes(){
//  var url="http://services.odata.org/V4/Northwind/Northwind.svc/Customers?$filter=Country eq 'Germany'";
var url="http://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request =new XMLHttpRequest();
  request.onreadystatechange=function()
  {
    if (this.readyState == 4 && this.status ==200){
      clientesObtenidos=request.responseText;
      procesarClientes();
    }
  }
  request.open("GET",url,true);
  request.send();
}

function   procesarClientes(){
  var JSONClientes = JSON.parse(clientesObtenidos);
  var tabla = document.getElementById("tablaClientes");
  for (var i =0; i< JSONClientes.value.length;i++){
    var nuevaFila = document.createElement("tr");

    var columnaCustomerID = document.createElement("td");
    columnaCustomerID.innerText=JSONClientes.value[i].CustomerID;

    var columnaContactName = document.createElement("td");
    columnaContactName.innerText=JSONClientes.value[i].ContactName;

    var country = JSONClientes.value[i].Country;
    var columnaCountry = document.createElement("td");
    columnaCountry.innerText=country;


    var imageFlag = document.createElement("img");
    var url=flagUrl+country+".png";
  //  imageFlag.setAttribute("src", url);
    imageFlag.src=url;
    //imageFlag.height=50;
    //imageFlag.width=70;
    imageFlag.classList.add("flag");
  //  imageFlag.setAttribute("height", "50");
  //  imageFlag.setAttribute("width", "70");
    var columnaFlag = document.createElement("td");
    columnaFlag.appendChild(imageFlag);


    nuevaFila.appendChild(columnaCustomerID);
    nuevaFila.appendChild(columnaContactName);
    nuevaFila.appendChild(columnaCountry);
    nuevaFila.appendChild(columnaFlag);
    tabla.appendChild(nuevaFila);
    //console.log(JSONProductos.value[i].ProductName);
  }
}
